//Uso el MEGA 2560

//Vumetro de 5 leds que cambia mediante un preset

//El ADC es de 10 bits, por lo que el maximo valor va a ser 1024

#define LED1 22
#define LED2 24
#define LED3 26
#define LED4 28
#define LED5 30
#define PRESET A0

int preset = 0;

void setup() 
{
  // put your setup code here, to run once:
  
  pinMode (LED1, OUTPUT);
  pinMode (LED2, OUTPUT);
  pinMode (LED3, OUTPUT);
  pinMode (LED4, OUTPUT);
  pinMode (LED5, OUTPUT);

  Serial.begin(9600);   //Con esto veo las variables en la compu

  //El ADC no se configura aca, se usa directamente en el void loop()  
}

void loop() 
{
  // put your main code here, to run repeatedly:

  preset = analogRead (PRESET);   //Leo el valor del preset

  Serial.print ("Lectura: ");   //Envio strings a la compu
  Serial.println (preset);      //Envio variables a la compu
  delay(200);

  if (preset < 204)
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, LOW);
    digitalWrite (LED3, LOW);
    digitalWrite (LED4, LOW);
    digitalWrite (LED5, LOW);
  }

  if ((preset < 408) && (preset > 203))
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, HIGH);
    digitalWrite (LED3, LOW);
    digitalWrite (LED4, LOW);
    digitalWrite (LED5, LOW);
  }

  if ((preset < 612) && (preset > 407))
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, HIGH);
    digitalWrite (LED3, HIGH);
    digitalWrite (LED4, LOW);
    digitalWrite (LED5, LOW);
  }

  if ((preset < 816) && (preset > 611))
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, HIGH);
    digitalWrite (LED3, HIGH);
    digitalWrite (LED4, HIGH);
    digitalWrite (LED5, LOW);
  }

  if (preset > 815)
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, HIGH);
    digitalWrite (LED3, HIGH);
    digitalWrite (LED4, HIGH);
    digitalWrite (LED5, HIGH);
  }

}
